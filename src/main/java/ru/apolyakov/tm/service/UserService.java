package ru.apolyakov.tm.service;

import ru.apolyakov.tm.api.repository.IUserRepository;
import ru.apolyakov.tm.api.service.IUserService;
import ru.apolyakov.tm.enumerated.Role;
import ru.apolyakov.tm.exception.empty.*;
import ru.apolyakov.tm.exception.user.*;
import ru.apolyakov.tm.model.User;
import ru.apolyakov.tm.util.HashUtil;

import java.util.List;

import static ru.apolyakov.tm.util.CheckUtil.isEmpty;

public class UserService extends AbstractService<User> implements IUserService {

    private final IUserRepository userRepository;

    public UserService(final IUserRepository userRepository) {
        super(userRepository);
        this.userRepository = userRepository;
    }

    @Override
    public User create(final String login, final String password) {
        if (isEmpty(login)) throw new EmptyLoginException();
        if (isEmpty(password)) throw new EmptyPasswordException();
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(password);
        userRepository.add(user);
        return user;
    }

    @Override
    public User findById(final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        return userRepository.findById(id);
    }


    @Override
    public User create(final String login, final String password, final String email) {
        if (isEmpty(login)) throw new EmptyLoginException();
        if (isEmpty(password)) throw new EmptyPasswordException();
        if (isEmpty(email)) throw new EmptyEmailException();
        if (userRepository.existsByLogin(login)) throw new LoginExistsException();
        if (userRepository.existsByEmail(email)) throw new EmailExistsException();
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        user.setEmail(email);
        userRepository.add(user);
        return user;
    }

    @Override
    public User create(final String login, final String password, final Role role) {
        if (isEmpty(login)) throw new EmptyLoginException();
        if (isEmpty(password)) throw new EmptyPasswordException();
        if (userRepository.existsByLogin(login)) throw new LoginExistsException();
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        user.setRole(role);
        userRepository.add(user);
        return user;
    }

    @Override
    public boolean existsByEmail(final String email) {
        return userRepository.existsByEmail(email);
    }

    @Override
    public boolean existsByLogin(final String login) {
        return userRepository.existsByLogin(login);
    }

    @Override
    public User findByLogin(final String login) {
        if (isEmpty(login)) throw new EmptyLoginException();
        return userRepository.findByLogin(login);
    }

    @Override
    public void removeByLogin(final String userId, final String login) {
        if (isEmpty(login)) throw new EmptyLoginException();
        userRepository.removeByLogin(userId, login);
    }

    @Override
    public void setPassword(final String userId, final String password) {
        if (isEmpty(userId)) throw new EmptyIdException();
        if (isEmpty(password)) throw new EmptyPasswordException();
        if (userRepository.findOneById(userId, userId) == null) throw new UserNotFoundException();
        userRepository.setPasswordById(userId, userId, password);
    }

    @Override
    public User setRole(final Role role, final String userId) {
        final User user = findById(userId);
        if (user == null) throw new UserNotFoundException();
        user.setRole(role);
        return user;
    }

    @Override
    public User updateUser(final String userId, final String firstName, final String lastName, final String middleName) {
        final User user = userRepository.findById(userId);
        user.setFirstName(firstName);
        user.setMiddleName(middleName);
        user.setLastName(lastName);
        return user;
    }

}
