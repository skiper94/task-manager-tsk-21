package ru.apolyakov.tm.service;

import ru.apolyakov.tm.api.repository.IProjectRepository;
import ru.apolyakov.tm.api.repository.ITaskRepository;
import ru.apolyakov.tm.api.service.IProjectTaskService;
import ru.apolyakov.tm.exception.empty.EmptyIdException;
import ru.apolyakov.tm.exception.entity.ProjectNotFoundException;
import ru.apolyakov.tm.model.Project;
import ru.apolyakov.tm.model.Task;

import java.util.List;

import static ru.apolyakov.tm.util.CheckUtil.isEmpty;


public class ProjectTaskService implements IProjectTaskService {

    private ITaskRepository taskRepository;

    private IProjectRepository projectRepository;

    public ProjectTaskService(IProjectRepository projectRepository, ITaskRepository taskRepository){
        this.taskRepository = taskRepository;
    }

    @Override
    public List<Task> findAllTaskByProjectId(final String projectId, final String userId) {
        if (isEmpty(projectId)) throw new ProjectNotFoundException();
        return taskRepository.findAllTaskByProjectId(projectId, userId);
    }

    @Override
    public void bindTaskByProjectId(final String projectId, final String taskId,  final String userId) {
        if (isEmpty(projectId)) throw new ProjectNotFoundException();
        if(isEmpty(taskId)) throw new EmptyIdException();
        taskRepository.bindTaskByProject(projectId, taskId, userId);
    }

    @Override
    public Task unbindTaskFromProject(final String taskId,final String userId) {
        if (isEmpty(taskId)) throw new EmptyIdException();
        return taskRepository.unbindTaskByProjectId(taskId,userId);
    }

    @Override
    public void removeTasksByProjectId(final String projectId,final String userId) {
        if (isEmpty(projectId)) throw new EmptyIdException();
        taskRepository.removeAllTaskByProjectId(projectId,userId);
    }

    @Override
    public void removeProjectById(final String projectId,final String userId){
        if (isEmpty(projectId)) throw new EmptyIdException();
        projectRepository.removeOneById(projectId,userId);
    }

}
