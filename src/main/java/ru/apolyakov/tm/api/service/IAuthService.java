package ru.apolyakov.tm.api.service;

import ru.apolyakov.tm.model.User;

public interface IAuthService {

    String getUserId();

    User getUser();

    boolean isAuth();

    void logout();

    void login(String login, String password);

    void registry(String login, String password, String email);

}
