package ru.apolyakov.tm.api.service;

import ru.apolyakov.tm.enumerated.Role;
import ru.apolyakov.tm.model.User;

import java.util.List;

public interface IUserService {

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User findById(String id);

    boolean existsByEmail(String email);

    boolean existsByLogin(String login);

    User findByLogin(String login);

    void removeByLogin(String login, String userId);

    void setPassword(String userId, String password);

    User setRole(Role role, String userId);

    User updateUser(
            String id,
            String firstName,
            String lastName,
            String middleName
    );

}
