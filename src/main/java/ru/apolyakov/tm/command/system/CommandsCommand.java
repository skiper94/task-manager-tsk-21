package ru.apolyakov.tm.command.system;

import ru.apolyakov.tm.command.AbstractCommand;

import java.util.Collection;

public final class CommandsCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-c";
    }

    @Override
    public String name() {
        return "commands";
    }

    @Override
    public String description() {
        return "Show all commands";
    }

    @Override
    public void execute() {
        System.out.println("[COMMANDS]");
        final Collection<AbstractCommand> commands = serviceLocator.getCommandService().getCommands();
        for (final AbstractCommand command : commands) {
            final String name = command.name();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

}
