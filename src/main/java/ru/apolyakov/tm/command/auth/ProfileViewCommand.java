package ru.apolyakov.tm.command.auth;

import ru.apolyakov.tm.model.User;

public final class ProfileViewCommand extends AbstractAuthCommand{

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "view-profile";
    }

    @Override
    public String description() {
        return "About profile";
    }

    @Override
    public void execute() {
        User user = serviceLocator.getAuthService().getUser();
        System.out.println("Login: " + user.getLogin());
        System.out.println("Email: " + user.getEmail());
        System.out.println("First name: " + user.getFirstName());
        System.out.println("Last name: " + user.getLastName());
        System.out.println("Middle name: " + user.getMiddleName());
    }
}
