package ru.apolyakov.tm.command.project;

import ru.apolyakov.tm.enumerated.Sort;
import ru.apolyakov.tm.model.Project;
import ru.apolyakov.tm.util.TerminalUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class ProjectListCommand extends AbstractProjectCommand{

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-list";
    }

    @Override
    public String description() {
        return "Show project list";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECTS LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        final String sort = TerminalUtil.nextLine();
        List<Project> projects = new ArrayList<>();
        if (sort == null || sort.isEmpty()) projects = serviceLocator.getProjectService().findAll(serviceLocator.getAuthService().getUserId());
        else {
            final Sort sortType = Sort.valueOf(sort);
            System.out.println(sortType.getDispayName());
            projects = serviceLocator.getProjectService().findAll(sortType.getComparator(), serviceLocator.getAuthService().getUserId());
        }
        int index = 1;
        for (final Project project: projects){
            System.out.println(index + ". " + project);
            index++;
        }
    }

}
