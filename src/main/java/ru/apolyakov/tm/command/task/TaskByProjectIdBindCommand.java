package ru.apolyakov.tm.command.task;

import ru.apolyakov.tm.exception.entity.TaskNotFoundException;
import ru.apolyakov.tm.model.Task;
import ru.apolyakov.tm.util.TerminalUtil;

public final class TaskByProjectIdBindCommand extends AbstractTaskCommand{

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-bind";
    }

    @Override
    public String description() {
        return "Bind task by project id";
    }

    @Override
    public void execute() {
        System.out.println("[BINDING TASK BY PROJECT ID]");
        System.out.println("to find the project id use the command: project-list");
        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID:");
        System.out.println("to find the task id use the command: task-list");
        final String taskId = TerminalUtil.nextLine();
        serviceLocator.getProjectTaskService().bindTaskByProjectId(projectId, taskId, serviceLocator.getAuthService().getUserId());
        System.out.println("[TASK ADDED TO PROJECT SUCCESSFUL]");
    }
}
