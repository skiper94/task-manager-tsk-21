package ru.apolyakov.tm.exception.empty;

import ru.apolyakov.tm.exception.AbstractException;

public class EmptyIdException extends AbstractException {

    public EmptyIdException() {
        super("ID is empty...");
    }

    public EmptyIdException(String value) {
        super("Error! " + value + " ID is empty.");
    }

}
