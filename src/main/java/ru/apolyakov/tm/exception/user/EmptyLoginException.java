package ru.apolyakov.tm.exception.user;

import ru.apolyakov.tm.exception.AbstractException;

public class EmptyLoginException extends AbstractException {

    public EmptyLoginException() {
        super("Error. Login is empty");
    }

}
