package ru.apolyakov.tm.exception.user;

import ru.apolyakov.tm.exception.AbstractException;

public class EmailExistsException extends AbstractException {

    public EmailExistsException() {
        super("Error! Email is already in use...");
    }

    public EmailExistsException(final String email){
        super("Error! Email" + email + "already in use...");
    }

}
