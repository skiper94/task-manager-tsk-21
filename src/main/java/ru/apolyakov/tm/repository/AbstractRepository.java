package ru.apolyakov.tm.repository;

import ru.apolyakov.tm.api.IRepository;
import ru.apolyakov.tm.exception.entity.EntityNotFoundException;
import ru.apolyakov.tm.model.AbstractEntity;
import ru.apolyakov.tm.model.Project;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    protected final List<E> entities = new ArrayList<>();

    @Override
    public List<E> findAll(final String userId) {
        return entities.stream()
                .filter(e -> userId.equals(e.getUserId()))
                .collect(Collectors.toList());
    }

    @Override
    public List<E> findAll(final Comparator<E> comparator, final String userId) {
        return entities.stream()
                .filter(e -> userId.equals(e.getUserId()))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    public E findOneById(final String id, final String userId) {
        return entities.stream()
                .filter(e -> userId.equals(e.getUserId()) && id.equals(e.getId()))
                .findFirst().orElse(null);
    }

    @Override
    public void clear(final String userId) {
        entities.stream()
                .filter(e -> userId.equals(e.getUserId()))
                .forEach(entities::remove);
    }

    @Override
    public void removeOneById(final String id, final String userId){
        entities.stream()
                .filter(e -> userId.equals(e.getUserId()) && id.equals(e.getId()))
                .findFirst()
                .ifPresent(this::remove);
    }

    @Override
    public void add(final E e) {
        entities.add(e);
    }

    @Override
    public void remove(final E e) {
        entities.remove(e);
    }

}
