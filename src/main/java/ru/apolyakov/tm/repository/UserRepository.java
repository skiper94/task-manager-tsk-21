package ru.apolyakov.tm.repository;

import ru.apolyakov.tm.api.repository.IUserRepository;
import ru.apolyakov.tm.model.User;
import ru.apolyakov.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.List;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public boolean existsByEmail(final String email) {
        return entities.stream()
                .anyMatch(e -> email.equals(e.getEmail()));
    }

    @Override
    public User findByLogin(final String login) {
        return entities.stream()
                .filter(e -> login.equals(e.getLogin()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public User findById(final String id) {
        return entities.stream()
                .filter(e -> id.equals(e.getId()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public User findByEmail(final String email) {
        return entities.stream()
                .filter(e -> email.equals(e.getEmail()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public void removeByLogin(final String userId, final String login) {
        remove(entities.stream()
                .filter(e -> userId.equals(e.getUserId()) && login.equals(e.getLogin()))
                .findFirst()
                .orElse(null)
        );
    }

    @Override
    public boolean existsByLogin(final String login) {
        return entities.stream()
                .anyMatch(e -> login.equals(e.getLogin()));
    }

    @Override
    public void setPasswordById(final String userId, final String id, final String password) {
        findOneById(id, userId).setPasswordHash(HashUtil.salt(password));
    }


}
