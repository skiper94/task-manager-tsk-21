package ru.apolyakov.tm.repository;

import ru.apolyakov.tm.api.repository.ITaskRepository;
import ru.apolyakov.tm.model.AbstractEntity;
import ru.apolyakov.tm.model.Project;
import ru.apolyakov.tm.model.Task;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static ru.apolyakov.tm.util.CheckUtil.isEmpty;

public class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    public Task findOneByIndex(final Integer index, final String userId){
        return entities.stream()
                .filter(e -> userId.equals(e.getUserId()))
                .skip(index - 1)
                .findFirst()
                .orElse(null);
    }

    @Override
    public Task findOneByName(final String name, final String userId){
        return entities.stream()
                .filter(e -> userId.equals(e.getUserId()) && name.equals(e.getName()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public Task removeOneByIndex(final Integer index, final String userId){
        final Task task = findOneByIndex(index, userId);
        if (task == null) return null;
        remove(task);
        return task;
    }

    @Override
    public Task removeOneByName(final String name, final String userId){
        final Task task = findOneByName(name, userId);
        if (task == null) return null;
        remove(task);
        return task;
    }

    @Override
    public List<Task> findAllTaskByProjectId(final String projectId, final String userId) {
        return entities.stream()
                .filter(e -> userId.equals(e.getUserId()) && projectId.equals(e.getProjectId()))
                .collect(Collectors.toList());
    }

    @Override
    public void removeAllTaskByProjectId(final String projectId, final String userId) {
        entities.stream()
                .filter(e -> userId.equals(e.getUserId()) && projectId.equals(e.getProjectId()))
                .forEach(this::remove);
    }

    @Override
    public void bindTaskByProject(final String projectId, final String taskId, final String userId) {
        entities.stream()
                .filter(e -> userId.equals(e.getUserId()) && taskId.equals(e.getId()))
                .findFirst()
                .ifPresent(e -> e.setProjectId(projectId));
    }

    @Override
    public Task unbindTaskByProjectId(final String taskId, final String userId) {
        final Task task = findOneById(taskId, userId);
        if (task == null) return null;
        task.setProjectId(null);
        return task;
    }

}
